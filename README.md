# argocd-lab
## 简介
### 参考
[ArgoCD 简明教程](https://blog.csdn.net/cr7258/article/details/122028096)
[ArgoCD GIT仓库地址](https://github.com/argoproj/argo-cd/releases/)
[ArgoCD CLI安装方式](https://argo-cd.readthedocs.io/en/stable/cli_installation/)
### 部署文件
```
# 应用部署
myapp-deployment.yaml
# 服务部署
myapp-service.yaml
```
### 相关文件
```
```
## ArgoCD CLI安装方式
### 官方推荐安装
```
curl -sSL -o argocd-linux-amd64 https://github.com/argoproj/argo-cd/releases/latest/download/argocd-linux-amd64
sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
rm argocd-linux-amd64
```
### 安装文件保存到本地安装(amd64 or arm64)
```
###  可以先保存到类似目录：/root/soft/argocd/argocd-linux-amd64

# sudo install -m 555 argocd-linux-amd64 /usr/local/bin/argocd
[root@node3 argocd]# ls /usr/local/bin/
argocd  docker-compose  etcd  etcdctl  helm  kubeadm  kubectl  kubelet  kube-scripts
```